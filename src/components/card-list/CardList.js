import React from "react";
import styles from "./CardList.module.css";
import Card from "../card/Card";
import Header from "../header/Header";

class CardList extends React.Component {
    state = {
        cards: []
    }

    //Executes when the HTML has been loaded into the DOM
    componentDidMount() {
        fetch("https://rickandmortyapi.com/api/character/").then(resp => resp.json()).then(resp => {
            this.setState({
                cards: [...resp.results]
            });
        }).catch(error => {
            console.log(error);
        })
    }

    render() {

        let cardComponents = null;
        
        if(this.state.cards.length > 0){
            //Build an array of card components
            cardComponents = this.state.cards.map(card => {
                return <Card card={card} key={card.id}/>
            });
        }
        else{
            //Set a loading message
            cardComponents = <p>Loading Rick and Morty...</p>;
        }

        return (
            <React.Fragment>
                <Header/>
                <h4>Rick and Morty Characters</h4>

                <div className={styles.CardList}>
                { cardComponents }
                </div>

            </React.Fragment>
        );
    }
}

export default CardList;