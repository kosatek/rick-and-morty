import React from "react";

//Functional/Presentation component
function Header(props) {
    return (
        <header>
            <h1>Rick and Morty</h1>
        </header>
    )
}

export default Header;