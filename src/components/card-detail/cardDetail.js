import React from "react";
import styles from "./cardDetail.module.css";
import Header from "../header/Header";

class CardDetail extends React.Component {

    //Executes when the HTML has been loaded into the DOM
    state = {
        card: null
    }

    componentDidMount() {
        fetch(`https://rickandmortyapi.com/api/character/${this.props.match.params.id}`).then(resp => resp.json()).then(resp => {
            this.setState({
                card: resp
            });
        }).catch(error => {
            console.log(error);
        })
    }

    render() {

        if (this.state.card) {
            return (
                <React.Fragment>
                    <Header />
                    <div className={styles.cardDetail}>
                        <img src={this.state.card.image} alt="Profile Picture"></img>
                        <h1>{this.state.card.name}</h1>
                        <p>Status: <span>
                            {this.state.card.status}
                        </span></p>
                        <p>Species: <span>
                            {this.state.card.species}
                        </span></p>
                        <p>Gender: <span>
                            {this.state.card.gender}
                        </span></p>
                        <p>Origin: <span>
                            {this.state.card.origin.name}
                        </span></p>
                        <p>Location: <span>
                            {this.state.card.location.name}
                        </span></p>
                        <p>Episodes: <span>
                            {this.state.card.episode.map(epi => 
                            <li> {epi} </li>
                                )}
                        </span></p>
                        <p>Created At: <span>
                            {this.state.card.created}
                        </span></p>
                    </div>
                </React.Fragment>
            );
        }
        else {
            return (
                <React.Fragment>
                    <Header></Header>
                    <p>Loading Profile...</p>
                </React.Fragment>
            )
        }
    }

}

export default CardDetail;