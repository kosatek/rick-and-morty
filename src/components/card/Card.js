import React from "react";
import { Link } from "react-router-dom";
import styles from "./Card.module.css"

function Card(props) {

    const { card } = props;

    return (
        <div className={styles.Card}>
            <img src={card.image} alt={card.name}></img>
            <h4>{card.name}</h4>
            <div className={styles.Stats}>
                <p>Status: <span>
                    {card.status}
                </span></p>
                <p>Species: <span>
                    {card.species}
                </span>
                </p>
                <p>Gender: <span>
                    {card.gender}
                </span></p>
            </div>
            <button><Link to={"card-detail/" + card.id}>View More</Link></button>
        </div>
    );
}
export default Card;